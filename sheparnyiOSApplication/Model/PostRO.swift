//
//  PostRO.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

struct PostRO: IdentifiableProtocol {
    let id: Int32
    let title: String
    let body: String
    let userId: Int32
}
