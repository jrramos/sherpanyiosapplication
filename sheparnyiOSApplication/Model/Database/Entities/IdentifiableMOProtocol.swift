//
//  IdentifiableMOProtocol.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 12/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

protocol IdentifiableProtocol {
    var id: Int32 { get }
}

protocol IdentifiableMOProtocol: class, IdentifiableProtocol {
    static var repositoryIdentifier: AppRepostitoryMOIdentifiers { get }
}
