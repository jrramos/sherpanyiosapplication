//
//  UserMO+CoreDataProperties.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation
import CoreData


extension UserMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserMO> {
        return NSFetchRequest<UserMO>(entityName: "User")
    }

    @NSManaged public var email: String
    @NSManaged public var id: Int32
    @NSManaged public var name: String
    @NSManaged public var phone: String
    @NSManaged public var username: String
    @NSManaged public var website: String
    @NSManaged public var address: AddressMO
    @NSManaged public var albums: NSSet
    @NSManaged public var company: CompanyMO
    @NSManaged public var posts: NSSet
}

// MARK: Generated accessors for albums
extension UserMO {

    @objc(addAlbumsObject:)
    @NSManaged public func addToAlbums(_ value: AlbumMO)

    @objc(removeAlbumsObject:)
    @NSManaged public func removeFromAlbums(_ value: AlbumMO)

    @objc(addAlbums:)
    @NSManaged public func addToAlbums(_ values: NSSet)

    @objc(removeAlbums:)
    @NSManaged public func removeFromAlbums(_ values: NSSet)

}

// MARK: Generated accessors for posts
extension UserMO {

    @objc(addPostsObject:)
    @NSManaged public func addToPosts(_ value: PostMO)

    @objc(removePostsObject:)
    @NSManaged public func removeFromPosts(_ value: PostMO)

    @objc(addPosts:)
    @NSManaged public func addToPosts(_ values: NSSet)

    @objc(removePosts:)
    @NSManaged public func removeFromPosts(_ values: NSSet)

}
