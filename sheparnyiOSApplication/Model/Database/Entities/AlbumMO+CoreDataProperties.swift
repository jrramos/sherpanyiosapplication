//
//  AlbumMO+CoreDataProperties.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation
import CoreData


extension AlbumMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AlbumMO> {
        return NSFetchRequest<AlbumMO>(entityName: "Album")
    }

    @NSManaged public var id: Int32
    @NSManaged public var title: String
    @NSManaged public var photos: NSSet
    @NSManaged public var user: UserMO
}

// MARK: Generated accessors for photos
extension AlbumMO {

    @objc(addPhotosObject:)
    @NSManaged public func addToPhotos(_ value: PhotoMO)

    @objc(removePhotosObject:)
    @NSManaged public func removeFromPhotos(_ value: PhotoMO)

    @objc(addPhotos:)
    @NSManaged public func addToPhotos(_ values: NSSet)

    @objc(removePhotos:)
    @NSManaged public func removeFromPhotos(_ values: NSSet)

}
