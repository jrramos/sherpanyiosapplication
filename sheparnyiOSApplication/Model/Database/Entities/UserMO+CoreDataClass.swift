//
//  UserMO+CoreDataClass.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation
import CoreData

@objc(UserMO)
public class UserMO: NSManagedObject, IdentifiableMOProtocol {
    static let repositoryIdentifier = AppRepostitoryMOIdentifiers.user
}
