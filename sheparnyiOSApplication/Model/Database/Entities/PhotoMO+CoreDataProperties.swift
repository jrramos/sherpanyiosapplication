//
//  PhotoMO+CoreDataProperties.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation
import CoreData


extension PhotoMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PhotoMO> {
        return NSFetchRequest<PhotoMO>(entityName: "Photo")
    }

    @NSManaged public var id: Int32
    @NSManaged public var thumbnailURL: String
    @NSManaged public var title: String
    @NSManaged public var url: String
    @NSManaged public var album: AlbumMO
}
