//
//  AddressMO+CoreDataProperties.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation
import CoreData

extension AddressMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AddressMO> {
        return NSFetchRequest<AddressMO>(entityName: "Address")
    }

    @NSManaged public var city: String
    @NSManaged public var lat: Float
    @NSManaged public var long: Float
    @NSManaged public var street: String
    @NSManaged public var suite: String
    @NSManaged public var zipCode: String
    @NSManaged public var user: UserMO

}
