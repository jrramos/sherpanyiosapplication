//
//  PostMO+CoreDataClass.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation
import CoreData

@objc(PostMO)
public class PostMO: NSManagedObject, IdentifiableMOProtocol {
    static let repositoryIdentifier = AppRepostitoryMOIdentifiers.post
}
