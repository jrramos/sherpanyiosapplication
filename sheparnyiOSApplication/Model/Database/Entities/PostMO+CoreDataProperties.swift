//
//  PostMO+CoreDataProperties.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation
import CoreData


extension PostMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PostMO> {
        return NSFetchRequest<PostMO>(entityName: "Post")
    }

    @NSManaged public var body: String
    @NSManaged public var id: Int32
    @NSManaged public var title: String
    @NSManaged public var user: UserMO
}
