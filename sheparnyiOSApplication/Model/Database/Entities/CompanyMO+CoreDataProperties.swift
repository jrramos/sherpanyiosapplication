//
//  CompanyMO+CoreDataProperties.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation
import CoreData


extension CompanyMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CompanyMO> {
        return NSFetchRequest<CompanyMO>(entityName: "Company")
    }

    @NSManaged public var bs: String
    @NSManaged public var catchPhrase: String
    @NSManaged public var name: String
    
    @NSManaged public var user: UserMO

}
