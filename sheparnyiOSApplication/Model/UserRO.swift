//
//  UserRO.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

struct AddressRO {
    let street: String
    let suite: String
    let city: String
    let zipCode: String
    let geo: (lat: Float, long: Float)
}

struct CompanyRO {
    let name: String
    let catchPhrase: String
    let bs: String
}

struct UserRO: IdentifiableProtocol {
    
    let id: Int32
    let name: String
    let username: String
    let email: String
    let address: AddressRO
    let phone: String
    let website: String
    let company: CompanyRO
}
