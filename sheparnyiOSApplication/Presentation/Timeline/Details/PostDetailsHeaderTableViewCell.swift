//
//  PostDetailsHeaderCollectionCell.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 17/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import UIKit

struct PostDetailsHeaderCollectionCellViewModel {
    let title: String
    let body: String
    
    init(postDetails: PostDetails) {
        self.title = postDetails.title
        self.body = postDetails.body
    }
}

class PostDetailsHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with viewModel: PostDetailsHeaderCollectionCellViewModel) {
        self.title.text = viewModel.title
        self.body.text = viewModel.body
    }
    
    
}
