//
//  PostEntryTableViewCell.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 13/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import UIKit

class PostEntryTableViewCell: UITableViewCell {

    @IBOutlet weak var postTitleLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with viewModel: PostEntryTableViewCellViewModel) {
        self.postTitleLabel.text = viewModel.postTitle
        self.emailLabel.text = viewModel.email
    }
    
}
