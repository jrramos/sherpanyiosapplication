//
//  PostEntryTableViewCellViewModel.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 13/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

struct PostEntryTableViewCellViewModel {
    
    let postEntry: PostEntry
    
    let postTitle: String
    let email: String
    
    init(postEntry: PostEntry) {
        self.postEntry = postEntry
        
        self.postTitle = postEntry.title
        self.email = postEntry.user.email
    }
    
}
