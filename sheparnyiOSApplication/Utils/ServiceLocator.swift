//
//  ServiceLocator.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 12/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

final class ServiceLocator {
    
    let serviceLocatorIdentifier = "sheparnyiOSApplication.serviceLocator"
    
    static let shared = ServiceLocator()
    
    private let queue: DispatchQueue
    private var services = [String: Any]()
    
    private init() {
        self.queue = DispatchQueue(label: serviceLocatorIdentifier)
    }
    
    func register<T>(service: T) {
        self.queue.sync {
            self.services[String(describing: T.self)] = service
        }
    }
    
    func unregisterService(ofType type: AnyObject.Type) {
        self.queue.sync {
            _ = self.services.removeValue(forKey: String(describing: type))
        }
    }
    
    func hasService(ofType type: AnyObject.Type) -> Bool {
        var hasService = false
        
        self.queue.sync {
            hasService = (self.services[String(describing: type)] != nil)
        }
        
        return hasService
    }
    
    func service<T>(ofType type: T.Type) -> T {
        
        var service: T? = nil
        self.queue.sync {
            service = self.services[String(describing: type)] as? T
        }
        
        guard let serviceUnwrapped = service else {
            fatalError("💥 You requested a service that does not exist! Type: \(type)")
        }
        
        return serviceUnwrapped
    }
}
