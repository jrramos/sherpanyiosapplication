//
//  Observable.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 16/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

final class Observable<T> {
    
    var value: T {
        didSet {
            self.valueChangedBlock?(self.value)
        }
    }
    
    private var valueChangedBlock: ((T) -> Void)?
    
    init(value: T) {
        self.value = value
    }
    
    func addObserver(valueChangedBlock: @escaping ((T) -> Void)) {
        self.valueChangedBlock = valueChangedBlock
    }
}
