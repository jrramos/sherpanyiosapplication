//
//  Dictionary+Sheparny.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

extension Dictionary {
    
    subscript(jsonDict key: Key) -> [String:Any] {
        get {
            return self[key] as! [String:Any]
        }
        set {
            self[key] = newValue as? Value
        }
    }
    
    subscript(string key: Key) -> String {
        get {
            return self[key] as! String
        }
        set {
            self[key] = newValue as? Value
        }
    }
    
    subscript(int32 key: Key) -> Int32 {
        get {
            return self[key] as! Int32
        }
        set {
            self[key] = newValue as? Value
        }
    }
    
    subscript(float key: Key) -> Float {
        get {
            return self[key] as! Float
        }
        set {
            self[key] = newValue as? Value
        }
    }
    
}
