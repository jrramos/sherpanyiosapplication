//
//  URLSessionProtocols.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

// These protocols aim to provide a convinient interface to test network requests

protocol URLSessionDataTaskProtocol {
    func resume()
}

protocol URLSessionProtocol {
    typealias DataTaskResult = (Data?, URLResponse?, Error?) -> Void
    
    func dataTask(withURL request: URL, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol
}

extension URLSessionDataTask: URLSessionDataTaskProtocol {}

extension URLSession: URLSessionProtocol {
    func dataTask(withURL request: URL, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
        return self.dataTask(with: request, completionHandler: completionHandler) as URLSessionDataTaskProtocol
    }
}
