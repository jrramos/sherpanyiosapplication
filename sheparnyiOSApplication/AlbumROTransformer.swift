//
//  AlbumTransformer.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

class AlbumTransformer {
    
    static func transform(json: JSONDictionary) -> AlbumRO {
        let id = json[int32: "id"]
        let title = json[string: "title"]
        let userId = json[int32: "userId"]
        
        return AlbumRO(id: id, title: title, userId: userId)
    }
    
}
