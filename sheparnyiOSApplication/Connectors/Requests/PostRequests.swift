//
//  File.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

struct PostRequests {
    
    struct URLPaths {
        static let allPosts = "posts/"
        
    }
    
    static let all = Resource<[PostRO]>(urlPath: URLPaths.allPosts, cachable: false, parseJSON: { json in
        guard let dictionaries = json as? [JSONDictionary] else { return nil }
        return dictionaries.flatMap(PostTransformer.transform)
    })
}
