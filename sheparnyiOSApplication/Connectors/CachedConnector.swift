//
//  CachedConnector.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 17/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

struct FileStorage {
    let baseURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    
    subscript(key: String) -> Data? {
        get {
            let url = baseURL.appendingPathComponent(key)
            return try? Data(contentsOf: url)
        }
        set {
            let url = baseURL.appendingPathComponent(key)
            _ = try? newValue?.write(to: url)
        }
    }
}

final class Cache {
    var storage = FileStorage()
    
    func load<A>(_ resource: Resource<A>) -> A? {
        let data = storage[resource.cacheKey]
        return data.flatMap(resource.parse)
    }
    
    func save<A>(_ data: Data, for resource: Resource<A>) {
        storage[resource.cacheKey] = data
    }
}

protocol ConnectorProtocol: class {
    
    var session: URLSessionProtocol { get }
    
    func load<A>(resource: Resource<A>, completion: @escaping (A?) -> ())
}

final class CachableConnector: ConnectorProtocol {
    let connector: ConnectorProtocol
    let session: URLSessionProtocol
    let cache = Cache()
    
    init(connector: ConnectorProtocol) {
        self.connector = connector
        self.session = connector.session
    }
    
    func load<A>(resource: Resource<A>, completion: @escaping (A?) -> ()) {
        if resource.cachable, let cachedValue = cache.load(resource) {
            print("cache hit")
            completion(cachedValue)
        }
        
        self.connector.load(resource: resource, completion: completion)
    }
}
