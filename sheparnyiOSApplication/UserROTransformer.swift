//
//  UserTransformer.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

class UserTransformer {
    
    static func transform(json: JSONDictionary) -> UserRO {
        let id = json[int32: "id"]
        let name = json[string: "name"]
        let username = json[string: "username"]
        let email = json[string: "email"]
        let address: AddressRO = self.transform(json: json[jsonDict: "address"])
        let phone = json[string: "phone"]
        let website  = json[string: "website"]
        let company: CompanyRO = self.transform(json: json[jsonDict: "company"])
        
        return UserRO(id: id,
                    name: name,
                    username: username,
                    email: email,
                    address: address,
                    phone: phone,
                    website: website,
                    company: company)
    }
    
    private static func transform(json: JSONDictionary) -> AddressRO {
        let street = json[string: "street"]
        let suite = json[string: "suite"]
        let city = json[string: "city"]
        let zipCode = json[string: "zipcode"]
        
        let geoJSON = json[jsonDict: "geo"]
        let geo = (Float(geoJSON[string: "lat"])!, Float(geoJSON[string: "lng"])!)
        
        return AddressRO(street: street, suite: suite, city: city, zipCode: zipCode, geo: geo)
    }
    
    private static func transform(json: JSONDictionary) -> CompanyRO {
        let name = json[string: "name"]
        let catchPhrase = json[string: "catchPhrase"]
        let bs = json[string: "bs"]
        
        return CompanyRO(name: name, catchPhrase: catchPhrase, bs: bs)
    }
    
}
