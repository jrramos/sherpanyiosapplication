//
//  PostDetailsViewController.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import UIKit

struct PostDetailsViewModel {
    
    private let postsStore = ServiceLocator.shared.service(ofType: PostStore.self)
    
    let postDetails: PostDetails
    
    init(postEntry: PostEntry) {
        self.postDetails = self.postsStore.postDetails(for: postEntry.id)
    }
    
}

class PostDetailsViewController: UITableViewController {

    var viewModel: PostDetailsViewModel? {
        didSet {
            guard self.viewModel != nil else { return }
            
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.estimatedRowHeight = 50 // just a placeholder
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.reloadData()
        
        self.tableView.tableFooterView = UIView()
        
        self.registerCells(in: self.tableView)
        // Do any additional setup after loading the view.
    }
    
    func registerCells(in tableView: UITableView) {
        let nibName = UINib(nibName: String(describing: PostDetailsHeaderTableViewCell.self), bundle:nil)
        
        tableView.register(nibName, forCellReuseIdentifier: String(describing: PostDetailsHeaderTableViewCell.self))
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard self.viewModel != nil else { return 0 }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PostDetailsHeaderTableViewCell.self), for: indexPath) as! PostDetailsHeaderTableViewCell
        guard let viewModel = self.viewModel else { fatalError() }
        
        let cellViewModel = PostDetailsHeaderCollectionCellViewModel(postDetails: viewModel.postDetails)
        cell.configure(with: cellViewModel)
        
        return cell
    }
    
}
