//
//  NSFetchRequest+Sherpany.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import CoreData

extension NSPredicate {
    
    static func predicate(with identifier: Int32) -> NSPredicate {
         return NSPredicate(format: "self.id == %d", identifier)
    }
    
}
