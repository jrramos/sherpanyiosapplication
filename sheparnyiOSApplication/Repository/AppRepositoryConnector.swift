//
//  AppRepositoryConnector.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import CoreData
import Foundation

class AppRepositoryConnector {
    
    fileprivate let appRepository: AppRepository
    
    init(appRepository: AppRepository) {
        self.appRepository = appRepository
    }
    
    // MARK: - Post
    
    @discardableResult
    func create(post: PostRO) -> PostMO {
        
        let configurationBlock: (PostMO) -> Void = { postMo in
            postMo.id = Int32(post.id)
            postMo.title = post.title
            postMo.body = post.body
            
            guard let user: UserMO = self.appRepository.fetchObjectWithIdentifier(.user, predicate: NSPredicate.predicate(with: post.userId)).first else {
                fatalError("You should have a user at this point...")
            }
            postMo.user = user
        }
        
        return appRepository.createObjectWithIdentifier(.post, configurationBlock: configurationBlock)
    }
    
    // MARK: - User
    
    @discardableResult
    func create(user: UserRO) -> UserMO {
        
        let configurationBlock: (UserMO) -> Void = { userMO in
            userMO.email = user.email
            userMO.id = Int32(user.id)
            userMO.name = user.name
            userMO.phone = user.phone
            userMO.username = user.username
            userMO.website = user.username
            userMO.address = self.create(address: user.address)
            userMO.company = self.create(company: user.company)
            userMO.albums = []
        }
        
        return appRepository.createObjectWithIdentifier(.user, configurationBlock: configurationBlock)
    }
    
    // MARK: - Company
    
    @discardableResult
    func create(company: CompanyRO) -> CompanyMO {
        
        let configurationBlock: (CompanyMO) -> Void = { companyMO in
            companyMO.name = company.name
            companyMO.catchPhrase = company.catchPhrase
            companyMO.bs = company.bs
        }
        
        return appRepository.createObjectWithIdentifier(.company, configurationBlock: configurationBlock)
    }
    
    
    // MARK: - Address
    
    @discardableResult
    func create(address: AddressRO) -> AddressMO {
        
        let configurationBlock: (AddressMO) -> Void = { addressMO in
            addressMO.street = address.street
            addressMO.suite = address.suite
            addressMO.city = address.city
            addressMO.zipCode = address.zipCode
            addressMO.lat = address.geo.lat
            addressMO.long = address.geo.long
        }
        
        return appRepository.createObjectWithIdentifier(.address, configurationBlock: configurationBlock)
    }
    
    // MARK: - Photo
    
    @discardableResult
    func create(photo: PhotoRO) -> PhotoMO {
        
        let configurationBlock: (PhotoMO) -> Void = { photoMO in
            photoMO.id = Int32(photo.id)
            photoMO.title = photo.title
            photoMO.thumbnailURL = photo.thumbnailURL.absoluteString
            
            guard let album: AlbumMO = self.appRepository.fetchObjectWithIdentifier(.album, predicate: NSPredicate.predicate(with: photo.albumId)).first else {
                fatalError("You should have a user at this point...")
            }
            
            photoMO.album = album
        }
        
        return appRepository.createObjectWithIdentifier(.photo, configurationBlock: configurationBlock)
    }
    
    // MARK: - Album
    
    @discardableResult
    func create(album: AlbumRO) -> AlbumMO {
        
        let configurationBlock: (AlbumMO) -> Void = { albumMO in
            albumMO.id = Int32(album.id)
            albumMO.title = album.title
            albumMO.photos = []
            
            guard let user: UserMO = self.appRepository.fetchObjectWithIdentifier(.user, predicate: NSPredicate.predicate(with: album.userId)).first else {
                fatalError("You should have a user at this point...")
            }
            
            albumMO.user = user
        }
        
       return appRepository.createObjectWithIdentifier(.album, configurationBlock: configurationBlock)
    }
    
}
