//
//  StoreProtocol.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 12/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

protocol StoreProtocol {    
    associatedtype ObjectType: IdentifiableProtocol
    associatedtype ObjectMOType: IdentifiableMOProtocol
    
    var appRepository: AppRepository { get }
    
    func create(object: ObjectType) -> ObjectMOType
    
    func filterAddedObjects(from: [ObjectType]) -> [ObjectType]
}

extension StoreProtocol {
    
    func filterAddedObjects(from objects: [ObjectType]) -> [ObjectType] {
        let currentIdentifiers = self.appRepository.fetchObjectIdentifiers(ObjectMOType.repositoryIdentifier)
        
        return objects.filter { !currentIdentifiers.contains($0.id) }
    }
    
}
