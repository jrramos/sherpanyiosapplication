//
//  PhotoStore.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 12/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

final class PhotoStore: StoreProtocol {
    
    let appRepository: AppRepository
    
    init(appRepository: AppRepository) {
        self.appRepository = appRepository
    }
    
    func create(object: PhotoRO) -> PhotoMO {
        
        let configurationBlock: (PhotoMO) -> Void = { photoMO in
            photoMO.id = Int32(object.id)
            photoMO.title = object.title
            photoMO.thumbnailURL = object.thumbnailURL.absoluteString
            
            guard let album: AlbumMO = self.appRepository.fetchObjectWithIdentifier(.album, predicate: NSPredicate.predicate(with: object.albumId)).first else {
                fatalError("You should have a user at this point...")
            }
            
            photoMO.album = album
        }
        
        return appRepository.createObjectWithIdentifier(.photo, configurationBlock: configurationBlock)
    }
}
