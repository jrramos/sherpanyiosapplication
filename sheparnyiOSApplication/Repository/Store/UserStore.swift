//
//  UserStore.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 12/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

final class UserStore: StoreProtocol {
    
    let appRepository: AppRepository
    
    init(appRepository: AppRepository) {
        self.appRepository = appRepository
    }
    
    func create(object: UserRO) -> UserMO {
        let address = self.create(address: object.address)
        let company = self.create(company: object.company)
        
        let configurationBlock: (UserMO) -> Void = { userMO in
            userMO.email = object.email
            userMO.id = Int32(object.id)
            userMO.name = object.name
            userMO.phone = object.phone
            userMO.username = object.username
            userMO.website = object.username
            userMO.address = address
            userMO.company = company
            userMO.albums = []
        }
        
        return appRepository.createObjectWithIdentifier(.user, configurationBlock: configurationBlock)
    }
    
    // MARK: - Company
    
    @discardableResult
    func create(company: CompanyRO) -> CompanyMO {
        
        let configurationBlock: (CompanyMO) -> Void = { companyMO in
            companyMO.name = company.name
            companyMO.catchPhrase = company.catchPhrase
            companyMO.bs = company.bs
        }
        
        return appRepository.createObjectWithIdentifier(.company, configurationBlock: configurationBlock)
    }
    
    
    // MARK: - Address
    
    @discardableResult
    func create(address: AddressRO) -> AddressMO {
        
        let configurationBlock: (AddressMO) -> Void = { addressMO in
            addressMO.street = address.street
            addressMO.suite = address.suite
            addressMO.city = address.city
            addressMO.zipCode = address.zipCode
            addressMO.lat = address.geo.lat
            addressMO.long = address.geo.long
        }
        
        return appRepository.createObjectWithIdentifier(.address, configurationBlock: configurationBlock)
    }
}
