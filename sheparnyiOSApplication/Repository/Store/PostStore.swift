//
//  PostStore.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 12/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation
import CoreData

final class PostStore: StoreProtocol {
    
    let appRepository: AppRepository
    
    init(appRepository: AppRepository) {
        self.appRepository = appRepository
    }
    
    func create(object: PostRO) -> PostMO {
        let configurationBlock: (PostMO) -> Void = { postMo in
            postMo.id = Int32(object.id)
            postMo.title = object.title
            postMo.body = object.body
            
            guard let user: UserMO = self.appRepository.fetchObjectWithIdentifier(.user, predicate: NSPredicate.predicate(with: object.userId)).first else {
                fatalError("You should have a user at this point...")
            }
            postMo.user = user
        }
        
        return appRepository.createObjectWithIdentifier(.post, configurationBlock: configurationBlock)
    }
    
    var postsInfos: [PostEntry] {
        return self.appRepository.fetchObjectWithIdentifier(.post, transformer: PostEntryTransformer.transform)
    }
    
    func postDetails(for postId: Int32) -> PostDetails {
        return self.appRepository.fetchObjectWithIdentifier(.post, predicate: NSPredicate.predicate(with: postId), transformer: PostDetailsTransformer.transform).first!
    }
    
    func deletePostInfo(with id: Int32) {
        self.appRepository.deleteManagedObject(.post, predicate: NSPredicate.predicate(with: id))
    }
}
