//
//  AlbumStore.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 12/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

final class AlbumStore: StoreProtocol {
    
    let appRepository: AppRepository
    
    init(appRepository: AppRepository) {
        self.appRepository = appRepository
    }
    
    func create(object: AlbumRO) -> AlbumMO {
        let configurationBlock: (AlbumMO) -> Void = { albumMO in
            albumMO.id = Int32(object.id)
            albumMO.title = object.title
            albumMO.photos = []
            
            guard let user: UserMO = self.appRepository.fetchObjectWithIdentifier(.user, predicate: NSPredicate.predicate(with: object.userId)).first else {
                fatalError("You should have a user at this point...")
            }
            
            albumMO.user = user
        }
        
        return appRepository.createObjectWithIdentifier(.album, configurationBlock: configurationBlock)
    }
}
