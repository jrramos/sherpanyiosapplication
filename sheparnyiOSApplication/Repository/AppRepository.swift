//
//  AppRepository.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

import CoreData
import Foundation

enum AppRepostitoryMOIdentifiers: String {
    case post = "Post"
    case user = "User"
    case photo = "Photo"
    case album = "Album"
    case address = "Address"
    case company = "Company"
}

class AppRepository {
    
    let managedObjectContext: NSManagedObjectContext = {
        // This resource is the same name as your xcdatamodeld contained in your project.
        guard let modelURL = Bundle.main.url(forResource: "DataModel", withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = psc
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docURL = urls[urls.endIndex-1]
        /* The directory the application uses to store the Core Data store file.
         This code uses a file named "DataModel.sqlite" in the application's documents directory.
         */
        
        let storeURL = docURL.appendingPathComponent("DataModel.sqlite")
        do {
            try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        } catch {
            fatalError("Error migrating store: \(error)")
        }
        
        return managedObjectContext
    }()
    
    
    func objectIdentifierFromIdentifier(_ identifier: String) -> NSManagedObjectID? {
        guard let objectURL = URL(string: identifier),
            let objectManagedId = self.managedObjectContext.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: objectURL) else {
                NSLog("Unnable to convert object ID(\(identifier)) to URL")
                return nil
        }
        
        return objectManagedId
    }
}

extension AppRepository {
    
    // MARK: - CRUD Operations
    
    func createObjectWithIdentifier<T: NSManagedObject>(_ identifier: AppRepostitoryMOIdentifiers, configurationBlock: @escaping (T) -> Void) -> T {
        var returnedManagedObject: T?
        
        self.managedObjectContext.performAndWait {
            let managedModel = NSEntityDescription.insertNewObject(forEntityName: identifier.rawValue, into: self.managedObjectContext) as! T
            configurationBlock(managedModel)
            
            do {
                try self.managedObjectContext.save()
                returnedManagedObject = managedModel
                NSLog("Created an object with description: \(managedModel.description)")
            } catch let error {
                NSLog("Failure to save object with description: \(error)")
            }
        }
        
        return returnedManagedObject!
    }
    
    func deleteManagedObject(_ identifier: AppRepostitoryMOIdentifiers, predicate: NSPredicate?) -> Void {
        let fetchedObjects: [NSManagedObject] = self.fetchObjectWithIdentifier(identifier, predicate: predicate)
        
        guard fetchedObjects.count > 0 else {
            NSLog("Didn't find objects to delete with predicate: \(String(describing: predicate))")
            return
        }
        
        self.managedObjectContext.performAndWait {
            for object in fetchedObjects {
                self.managedObjectContext.delete(object)
            }
            
            do {
                try self.managedObjectContext.save()
            } catch {
                NSLog("Failed to save managed object context")
            }
        }
    }
    
    func fetchObjectWithIdentifier<T: NSManagedObject, A>(_ identifier: AppRepostitoryMOIdentifiers, predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor] = [], transformer: ((T) -> A)? = nil) -> [A] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: identifier.rawValue)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = sortDescriptors
        
        do {
            let objects = try managedObjectContext.fetch(fetchRequest) as! [T]
            
            guard let transformerUnwrapped = transformer else {
                return objects as! [A]
            }
            
            return objects.map(transformerUnwrapped)
        } catch {
            NSLog("Failed to fetch objects with identifier: \(identifier)")
            return []
        }
    }
    
    func fetchObjectIdentifiers(_ identifier: AppRepostitoryMOIdentifiers) -> [Int32] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: identifier.rawValue)
        fetchRequest.propertiesToFetch = ["id"]
        
        do {
            return try (managedObjectContext.fetch(fetchRequest) as! [IdentifiableMOProtocol]).map { $0.id }
        } catch {
            NSLog("Failed to fetch objects with identifier: \(identifier)")
            return []
        }
    }
}
