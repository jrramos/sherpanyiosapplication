//
//  AlbumRequests.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 12/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

struct AlbumRequests {
    
    struct URLPaths {
        static let allPosts = "albums/"
    }
    
    static let all = Resource<[AlbumRO]>(urlPath: URLPaths.allPosts, cachable: false, parseJSON: { json in
        guard let dictionaries = json as? [JSONDictionary] else { return nil }
        return dictionaries.flatMap(AlbumTransformer.transform)
    })
}
