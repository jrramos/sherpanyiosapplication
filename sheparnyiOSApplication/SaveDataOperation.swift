//
//  SaveDataOperation.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 12/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

final class SaveDataOperation<T: IdentifiableProtocol, A: StoreProtocol>: Operation {
    
    let session: FetchDataOperation<T>.SaveDataOperationSession<T>
    let store: A
    
    init(session: FetchDataOperation<T>.SaveDataOperationSession<T>, store: A) {
        self.session = session
        self.store = store
    }
    
    override func main () {
        if self.isCancelled {
            return
        }
        
        let fetchedObjects: [A.ObjectType] = (self.session.results as? [A.ObjectType]) ?? []
        let filteredObjects = self.store.filterAddedObjects(from: fetchedObjects)
        
        filteredObjects.forEach {
            _ = self.store.create(object: $0)
        }
    }
}
