//
//  PhotoTransformer.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

class PhotoTransformer {
    
    static func transform(json: JSONDictionary) -> PhotoRO {
        let id = json[int32: "id"]
        let title = json[string: "title"]
        let url = URL(string: json[string: "url"])!
        let thumbnailURL = URL(string: json[string: "thumbnailUrl"])!
        let albumId = json[int32: "albumId"]
        
        return PhotoRO(id: id,
                      title: title,
                      url: url,
                      thumbnailURL: thumbnailURL,
                      albumId: albumId)
    }
    
}
