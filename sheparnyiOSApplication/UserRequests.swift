//
//  UserRequests.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 12/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

struct UserRequests {
    
    struct URLPaths {
        static let allPosts = "users/"
    }
    
    static let all = Resource<[UserRO]>(urlPath: URLPaths.allPosts, cachable: false, parseJSON: { json in
        guard let dictionaries = json as? [JSONDictionary] else { return nil }
        return dictionaries.flatMap(UserTransformer.transform)
    })
}
