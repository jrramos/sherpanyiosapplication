//
//  PostDetails.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 16/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

struct PostDetails {
    let id: Int32
    let title: String
    let body: String
}
