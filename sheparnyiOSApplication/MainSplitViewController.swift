//
//  MainSplitViewController.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import UIKit

final class MainSplitViewModel {
    
    private let postsStore = ServiceLocator.shared.service(ofType: PostStore.self)
    private let syncManager = ServiceLocator.shared.service(ofType: SyncManager.self)
    
    private(set) var shouldDisplaySpinner: Observable<Bool>
    
    init() {
        self.shouldDisplaySpinner = Observable<Bool>(value: self.syncManager.isSynching.value)
        
        self.syncManager.isSynching.addObserver { [weak self] value in
            self?.shouldDisplaySpinner.value = value
        }
    }
    
}

class MainSplitViewController: UISplitViewController {
    
    struct Layout {
        static let preferredColumnWidth: CGFloat = 200
    }
    
    let postsStore = ServiceLocator.shared.service(ofType: PostStore.self)
    let syncManager = ServiceLocator.shared.service(ofType: SyncManager.self)
    
    let viewModel = MainSplitViewModel()
    
    var postEntriesViewController: PostsEntriesTableViewController?
    var postDetailsViewController: PostDetailsViewController?
    
    var activityIndicatorView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureLayout()
        self.setupDelegates()
        self.setupLoadingState()
        self.postDetailsViewController = self.viewControllers[1] as? PostDetailsViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func configureLayout() {
        self.minimumPrimaryColumnWidth = Layout.preferredColumnWidth
        self.maximumPrimaryColumnWidth = Layout.preferredColumnWidth
        
        self.preferredDisplayMode = .allVisible
    }
    
    private func setupDelegates() {
        guard let primaryViewController = self.viewControllers.first as? UINavigationController,
              let postEntriesViewController = primaryViewController.viewControllers.first as? PostsEntriesTableViewController else {
            return
        }
        
        postEntriesViewController.delegate = self
        self.postEntriesViewController = postEntriesViewController
    }
    
    private func setupLoadingState() {
        if viewModel.shouldDisplaySpinner.value {
            self.insertActivityView()
        }
        
        self.viewModel.shouldDisplaySpinner.addObserver { value in
            if value {
                self.insertActivityView()
            } else {
                self.removeActivityView()
                self.postEntriesViewController?.reloadViewModels()
            }
        }
    }
    
    
    // MARK: - Auxiliar 
    
    func insertActivityView() {
        guard self.activityIndicatorView == nil else { return }
        
        let activityIndicatorView = UIView()
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorView.backgroundColor = UIColor.gray.withAlphaComponent(0.3)
        self.view.addSubview(activityIndicatorView)
        
        activityIndicatorView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        activityIndicatorView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        activityIndicatorView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        activityIndicatorView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorView.addSubview(activityView)
        activityView.centerXAnchor.constraint(equalTo: activityIndicatorView.centerXAnchor).isActive = true
        activityView.centerYAnchor.constraint(equalTo: activityIndicatorView.centerYAnchor).isActive = true
        activityView.center = activityIndicatorView.center
        activityView.startAnimating()
        
        self.activityIndicatorView = activityIndicatorView
    }
    
    func removeActivityView() {
        DispatchQueue.main.async {
            self.activityIndicatorView?.removeFromSuperview()
            self.activityIndicatorView = nil
        }
    }
}

extension MainSplitViewController: PostsEntriesTableViewControllerDelegate {
    
    func postsEntriesTableViewController(_ viewController: UIViewController, didSelectPostEntry postEntry: PostEntry) {
        let viewModel = PostDetailsViewModel(postEntry: postEntry)
        self.postDetailsViewController?.viewModel = viewModel
    }
    
}
