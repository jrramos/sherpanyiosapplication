//
//  PostsEntriesViewController.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import UIKit

struct PostsEntriesTableViewModel {
    
    private let postsStore = ServiceLocator.shared.service(ofType: PostStore.self)
    
    private(set) var rows: [PostEntryTableViewCellViewModel]
    
    init() {
        self.rows = self.postsStore.postsInfos.map(PostEntryTableViewCellViewModel.init)
    }
    
    mutating func deleteRow(at index: Int) {
        self.postsStore.deletePostInfo(with: self.rows[index].postEntry.id)
        self.rows.remove(at: index)
    }
}

protocol PostsEntriesTableViewControllerDelegate: class {
    func postsEntriesTableViewController(_ viewController: UIViewController, didSelectPostEntry postEntry: PostEntry)
}

final class PostsEntriesTableViewController: UITableViewController {
    
    var viewModel = PostsEntriesTableViewModel()
    
    weak var delegate: PostsEntriesTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Challenge Accepted!"
        
        self.registerCells(in: self.tableView)
        
        self.tableView.estimatedRowHeight = 50 // just a placeholder
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.reloadViewModels()
    }
    
    func reloadViewModels() {
        DispatchQueue.main.async {
            self.viewModel = PostsEntriesTableViewModel()
            self.tableView.reloadData()
        }
    }
    
    func registerCells(in tableView: UITableView) {
        let nibName = UINib(nibName: String(describing: PostEntryTableViewCell.self), bundle:nil)
        
        tableView.register(nibName, forCellReuseIdentifier: String(describing: PostEntryTableViewCell.self))
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let postEntry = self.viewModel.rows[indexPath.row].postEntry
        self.delegate?.postsEntriesTableViewController(self, didSelectPostEntry: postEntry)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.viewModel.rows.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PostEntryTableViewCell.self), for: indexPath) as! PostEntryTableViewCell
        cell.configure(with: self.viewModel.rows[indexPath.row])
        
        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.viewModel.deleteRow(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

}
