//
//  AppDelegate+Sherpany.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 12/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation
import UIKit

extension AppDelegate {
    
    func initializeAppInstances() {
        
        let appRepository = AppRepository()
        let userStore = UserStore(appRepository: appRepository)
        let postStore = PostStore(appRepository: appRepository)
        let albumStore = AlbumStore(appRepository: appRepository)
        let photoStore = PhotoStore(appRepository: appRepository)
        let feedConnector = FeedConnector()
        let cachableConnector = CachableConnector(connector: feedConnector)
        let syncManager = SyncManager(connector: cachableConnector,
                                      userStore: userStore,
                                      postStore: postStore,
                                      albumStore: albumStore,
                                      photoStore: photoStore)
        
        ServiceLocator.shared.register(service: appRepository)
        ServiceLocator.shared.register(service: userStore)
        ServiceLocator.shared.register(service: postStore)
        ServiceLocator.shared.register(service: albumStore)
        ServiceLocator.shared.register(service: photoStore)
        ServiceLocator.shared.register(service: feedConnector)
        ServiceLocator.shared.register(service: cachableConnector)
        ServiceLocator.shared.register(service: syncManager)
    }

    func initializeSync() {
        let syncManager = ServiceLocator.shared.service(ofType: SyncManager.self)
        syncManager.startSync()
    }
    
    func startStoryboard() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let mainView = UIStoryboard(name: "Main", bundle: nil)
        let viewcontroller : UIViewController = mainView.instantiateViewController(withIdentifier: "MainSplitViewController") as UIViewController
        self.window!.rootViewController = viewcontroller
        self.window?.makeKeyAndVisible()
    }
    
}
