//
//  PostTransformer.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

class PostTransformer {
    
    static func transform(json: JSONDictionary) -> PostRO {
        let id = json[int32: "id"]
        let title = json[string: "title"]
        let body = json[string: "body"]
        let userId = json[int32: "userId"]
        
        return PostRO(id: id,title: title,body: body,userId: userId)
    }
    
}
