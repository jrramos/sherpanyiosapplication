//
//  PostDetailsTransformer.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 16/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

class PostDetailsTransformer {
    
    static func transform(postMO: PostMO) -> PostDetails {
        let id = postMO.id
        let title = postMO.title
        let body = postMO.body
        
        return PostDetails(id: id, title: title, body: body)
    }
    
}
