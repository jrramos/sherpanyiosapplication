//
//  PostEntryTransformer.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 16/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

class PostEntryTransformer {
    
    static func transform(postMO: PostMO) -> PostEntry {
        let id = postMO.id
        let title = postMO.title
        let user = UserInfoTransformer.transform(userMO: postMO.user)
        
        return PostEntry(id: id, title: title, user: user)
    }
    
}
