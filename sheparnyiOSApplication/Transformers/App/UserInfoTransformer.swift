//
//  UserInfoTransformer.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 16/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

class UserInfoTransformer {
    
    static func transform(userMO: UserMO) -> UserInfo {
        let id = userMO.id
        let email = userMO.email
        
        return UserInfo(id: id, email: email)
    }
    
}
