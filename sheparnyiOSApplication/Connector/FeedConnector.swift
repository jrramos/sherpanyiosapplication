//
//  FeedConnector.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String: Any]

private let baseURL = URL(string: "http://jsonplaceholder.typicode.com/")!

struct Episode {
    let id: String
    let title: String
}


struct Resource<A> {
    let urlPath: String
    let cachable: Bool
    let parse: (Data) -> A?
}

extension Resource {
    init(urlPath: String, cachable: Bool, parseJSON: @escaping (Any) -> A?) {
        self.urlPath = urlPath
        self.cachable = cachable
        self.parse = { data in
            let json = try? JSONSerialization.jsonObject(with: data, options: [])
            return json.flatMap(parseJSON)
        }
    }
}

extension Resource {
    var cacheKey: String {
        return "cache" + self.urlPath
    }
}


final class FeedConnector: ConnectorProtocol {
    
    let session: URLSessionProtocol
    
    init(session: URLSessionProtocol = URLSession.shared) {
        self.session = session
    }
    
    func load<A>(resource: Resource<A>, completion: @escaping (A?) -> ()) {
        (session.dataTask(withURL: baseURL.appendingPathComponent(resource.urlPath)) { data, _, _ in
            guard let data = data else {
                completion(nil)
                return
            }
            completion(resource.parse(data))
        } as URLSessionDataTaskProtocol).resume()
    }
}
