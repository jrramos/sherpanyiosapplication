//
//  FetchDataOperation.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 12/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

final class FetchDataOperation<T: IdentifiableProtocol>: AsynchronousOperation {
    
    class SaveDataOperationSession<T> {
        var results: [T]!
    }
    
    let connector: ConnectorProtocol
    let resource: Resource<[T]>
    
    var session = SaveDataOperationSession<T>()
    
    init(connector: ConnectorProtocol, resource: Resource<[T]>) {
        self.connector = connector
        self.resource = resource
    }
    
    override func execute() {
        self.connector.load(resource: resource) { [weak self] result in
            self?.session.results = result
            
            self?.finish()
        }
    }
}
