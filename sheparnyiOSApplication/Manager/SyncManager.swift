//
//  SyncManager.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import Foundation

class SyncManager {
    
    let connector: ConnectorProtocol
    let userStore: UserStore
    let postStore: PostStore
    let albumStore: AlbumStore
    let photoStore: PhotoStore
    
    fileprivate lazy var syncQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "sheparnyiOSApplication.syncQueue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    private(set) var isSynching = Observable<Bool>(value: false)
    
    init(connector: ConnectorProtocol,
         userStore: UserStore,
         postStore: PostStore,
         albumStore: AlbumStore,
         photoStore: PhotoStore) {
        
        self.connector = connector
        
        self.userStore = userStore
        self.postStore = postStore
        self.albumStore = albumStore
        self.photoStore = photoStore
    }
    
    func startSync() {
        syncQueue.isSuspended = true
        
        self.syncQueue.addOperation { self.isSynching.value = true }
        
        let fetchUsersInfo = FetchDataOperation(connector: connector, resource: UserRequests.all)
        let saveUsersInfo = SaveDataOperation(session: fetchUsersInfo.session, store: self.userStore)
        
        let fetchPostsInfo = FetchDataOperation(connector: connector, resource: PostRequests.all)
        let savePostsInfo = SaveDataOperation(session: fetchPostsInfo.session, store: self.postStore)
        
        let fetchAlbumsInfo = FetchDataOperation(connector: connector, resource: AlbumRequests.all)
        let saveAlbumsInfo = SaveDataOperation(session: fetchAlbumsInfo.session, store: self.albumStore)
        
        let fetchPhotosInfo = FetchDataOperation(connector: connector, resource: PhotoRequests.all)
        let savePhotosInfo = SaveDataOperation(session: fetchPhotosInfo.session, store: self.photoStore)
        
        syncQueue.addOperations([
            fetchUsersInfo,
            saveUsersInfo,
            fetchPostsInfo,
            savePostsInfo,
            fetchAlbumsInfo,
            saveAlbumsInfo,
            fetchPhotosInfo,
            savePhotosInfo,
            ], waitUntilFinished: false)
        
        self.syncQueue.addOperation { self.isSynching.value = false }
        
        self.syncQueue.isSuspended = false
    }
    
}
