//
//  PostTransformerTests.swift
//  sheparnyiOSApplication
//
//  Created by José Ramos on 11/09/2017.
//  Copyright © 2017 José Ramos. All rights reserved.
//

import XCTest
@testable import sheparnyiOSApplication

class PostTransformerTests: XCTestCase {

    final class MockDataTask: URLSessionDataTaskProtocol {
        func resume() {}
    }
    
    final class MockURLSession: URLSessionProtocol {
        let dataTask = MockDataTask()

        let response = [
            [
                "userId": 1,
                "id": 1,
                "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
            ],
            [
                "userId": 1,
                "id": 2,
                "title": "qui est esse",
                "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
            ],
        ]
        
        func dataTask(withURL request: URL, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
            let data = try! JSONSerialization.data(withJSONObject: response, options: [])
            completionHandler(data, nil, nil)
            
            return dataTask
        }
    }
    
    let mockedURLSession = MockURLSession()
    
    func testJSONDataTransformation() {
        var posts: [Post]? = []
        FeedConnector.load(session: mockedURLSession, resource: PostRequests.all) { parsedPosts in
            posts = parsedPosts
        }
        
        guard let firstPost = posts?.first else {
            XCTFail("fist post unavailable")
            
            return
        }
        
        XCTAssertEqual(firstPost.userId, 1)
        XCTAssertEqual(firstPost.id, 1)
        XCTAssertEqual(firstPost.title, "sunt aut facere repellat provident occaecati excepturi optio reprehenderit")
        XCTAssertEqual(firstPost.body, "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto")
        
        guard let secondPost = posts?.last else {
            XCTFail("second post unavailable")
            
            return
        }
        
        XCTAssertEqual(secondPost.userId, 1)
        XCTAssertEqual(secondPost.id, 2)
        XCTAssertEqual(secondPost.title, "qui est esse")
        XCTAssertEqual(secondPost.body, "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla")
        
    }
    
}
